# jsora

General purpose layered raster graphics in the browser! This is a frontend/browser javascript library for reading,
 writing and rendering OpenRaster (ORA) files. 

Code Repository: https://gitlab.com/inklabapp/jsora

NPM: https://www.npmjs.com/package/jsora

Current OpenRaster standard: https://www.openraster.org/

# what is supported

* reading, extracting image data and metadata from ORA files
* writing (creating new and modifying) ORA files, with any path structure and metadata
* api support for layers / groups in a easy-to-use manner
* rendering ORA files and edits to a browser canvas, or to base64
* all blend modes in the current standard (as of March-20-2020)
* In browser GPU-accelerated rendering. Special thanks to the GPU.JS project 
(https://gpu.rocks/) for providing the groundwork for this improvement. 

# limitations

* Currently there is limited support for using "paths" (Linux style) to reference multiple layers with the same 
name/group. You would need to use ident or UUID for this. 

# why not use...?

* I took note of another library with similar goals; https://github.com/zsgalusz/ora.js . However, I ended up 
not liking it because of the seeming lack of layer / group structure support, and lack of recent updates. 

# help

* for 'new style', all of the useful exports come from src/index.js . The repository is also preconfigured with webpack.
* for 'old style', there will be a jsora.min.js file in the 'dist' dir built for use in a script tag after each release.
* There is a useful html file in examples/tutorial.html which makes use of all of the high level features 
with some commentary. In addition, please feel free to reach out anytime for assistance. I am actively working on 
both developing this library and the standard itself. 

